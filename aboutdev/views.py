from django.http import HttpResponse            # pragma: no cover
from django.shortcuts import render, redirect   # pragma: no cover
from django.urls import reverse
from homepage.views import home

def about(request):
    link_homepage = reverse('home')

    context = {
        'link_homepage' : link_homepage,
    }

    return render(request, 'aboutdev.html', context)