from django.apps import AppConfig  # pragma: no cover


class AboutdevConfig(AppConfig):  # pragma: no cover
    name = 'aboutdev'
