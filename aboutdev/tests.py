from django.test import TestCase, Client  # pragma: no cover
from django.urls import resolve, reverse           # pragma: no cover
from django.test import LiveServerTestCase
from datetime import datetime             # pragma: no cover
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

from .views import home
from .views import about

class AboutUnitTest(TestCase):
    
    def test_aboutdev_url(self):
        response = Client().get('/aboutdev/')
        self.assertEqual(response.status_code, 200)

    def test_aboutdev_using_template(self):
        response = Client().get('/aboutdev/')
        self.assertTemplateUsed(response, 'aboutdev.html')

    def test_aboutdev_using_about_func(self):
        found = resolve('/aboutdev/')
        self.assertEqual(found.func, about)


class FunctionalTestAbout(LiveServerTestCase):

    def setUp(self):
        super(FunctionalTestAbout, self).setUp()
        opt = Options()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        # self.selenium = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=opt)
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=opt)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_name_text_is_displayed(self):
        self.selenium.get(self.live_server_url+'/aboutdev/')

        header = self.selenium.find_element_by_id('nama')
        self.assertEquals(
            header.text,
            'IQRAR AGALOSI NUREYZA'
        )

    def test_dark_mode(self):
        self.selenium.get(self.live_server_url+'/aboutdev/')

        self.selenium.find_element_by_class_name('btn-dark').click()
        
        btn = self.selenium.find_elements_by_tag_name('button')
        self.assertIn(self.selenium.find_element_by_class_name('btn-primary'), btn)

    def test_accordion_is_working_and_exist(self):
        self.selenium.get(self.live_server_url+'/aboutdev/')

        self.selenium.find_element_by_class_name('btn-box').click()
        accordions = self.selenium.find_elements_by_class_name('content-box')

        self.assertTrue(len(accordions) != 0)
        
