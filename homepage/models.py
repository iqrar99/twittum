from django.db import models  # pragma: no cover
from datetime import datetime, timedelta  # pragma: no cover

# Create your models here.
class Tweet(models.Model):
    id = models.AutoField(primary_key = True)
    tweet_text  = models.CharField(max_length = 300)
    tweet_time = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return f'({self.id})'
