from django.http import HttpResponse            # pragma: no cover
from django.shortcuts import render, redirect   # pragma: no cover
from django.urls import reverse

from .models import Tweet                      
from .forms import TweetForm                   

def home(request):
    # form for tweet
    tweet_form = TweetForm(request.POST or None)
    if request.method == 'POST':
        if tweet_form.is_valid():
            tweet_form.save()

            return redirect('home')

    context = {
        'your_tweet' : getTweets(),
        'tweet_form' : tweet_form,
        'link_aboutdev' : reverse('about'),
    }

    return render(request, 'home.html', context)

def getTweets():
    return Tweet.objects.all()
