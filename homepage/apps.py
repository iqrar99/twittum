from django.apps import AppConfig #pragma: no cover


class HomepageConfig(AppConfig): #pragma: no cover
    name = 'homepage'
