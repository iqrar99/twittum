from django.test import TestCase, Client  # pragma: no cover
from django.urls import resolve, reverse           # pragma: no cover
from django.test import LiveServerTestCase
from datetime import datetime             # pragma: no cover
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

from .views import home
from .models import Tweet
from .views import home, getTweets
from .forms import TweetForm

class TwittumUnitTest(TestCase):

    def test_home_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    # Test Model

    def test_tweet_add(self):
        tweet_test = 'testing'
        Tweet.objects.create(tweet_text = tweet_test)
        self.assertEqual(len(Tweet.objects.all()), 1)

    def test_tweet_object_id(self):
        Tweet.objects.create(id = 1 ,tweet_text = 'testing')
        t = Tweet.objects.get(tweet_text = 'testing')

        self.assertEqual(t.__str__(), '(1)')

    # Test Form

    def test_form_validation_for_blank_items(self):
        form = TweetForm(data= {'tweet_text' : ''})
        self.assertFalse(form.is_valid())

    def test_form_validation_for_exceeding_tweet_length(self):
        form = TweetForm(data= {
            'tweet_text': 'EQSrYWwgaePrwPco2YCsD7SDQpU0PRL4g1Ns9lTB0a3s0mVeuw5ps2FR5CXutcO0cN5rouwNWmYRVcOxIR0QAPkTUN0XshcmhO1mRzhNA91BiKAAZeNi5zrRWeMu3Ff9GIPMJWP5gxrpzF2W2RHueCK8zpe0oJJz3HbFDeHwQkpFiLsohcNhNIl0pnVINUtFZNSQbimcdmykb2grTlznHHPdFT3srHJEyH8xG58m7NbEHvQjzvh8eLZKTbaw7Ij5pBMlMcm9FXUchF6eszWdyJsVroBqwelzGj28X87GyNhy55jaR'
        })
        self.assertFalse(form.is_valid())

    def test_form_validation_for_max_tweet_length(self):
        form = TweetForm(data = {
            'tweet_text': 'D2qjHRzGpWbNKDQfFkHRCQINnNB5pWCoEC0LVfZQgKcXipIac7cEjsgeoU78EKemjVjKcJvlQcR1LCYQkaLmdz0ncGfdmpGdycVUskR5fmDbhzhOJRXuSKDHB8TfSntvUwzRADVD8y5XmQzMos9hsi7uVX7eypsokiXKIJJDFmloE8XDwNV2Vz8t89ZFbHQmRKgJhi5RP7uIb5iGckI4ucBIij3jXtAUq6ok9rg03F1anClDwFK7w8T350qGTnKAC54TdzLgTIlSUwqwoyYCEaR0fM01DY0pnTOaYqSTDjff'
        })
        self.assertTrue(form.is_valid())

    # Test Views

    def test_getTweet_func(self):
        Tweet.objects.create(tweet_text = 'testing')
        self.assertEqual(len(getTweets()), 1)

    def test_post_error_and_render_the_result(self):
        test_tweet = 'testing'

        response_post = Client().post('/',{'tweet_text': ''})
        self.assertEqual(response_post.status_code, 200)
        
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test_tweet, html_response)


class FunctionalTestTweet(LiveServerTestCase):
    link = 'https://twittum.herokuapp.com/'

    def setUp(self):
        super(FunctionalTestTweet, self).setUp()
        opt = Options()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=opt)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_h1_text_is_displayed(self):
        self.selenium.get(self.live_server_url)

        header = self.selenium.find_element_by_class_name('twittum-header')
        self.assertEquals(
            header.find_element_by_tag_name('h1').text,
            'Hello, How do you do?'
        )

    def test_twittum_logo_is_exist(self):
        self.selenium.get(self.live_server_url)

        header = self.selenium.find_element_by_class_name('twittum-header')
        self.assertEquals(
            header.find_element_by_tag_name('img').get_attribute('alt'),
            'logo'
        )

    def test_user_sees_tweet_list(self):
        new_tweet = Tweet.objects.create(tweet_text = 'Good Morning!')
        self.selenium.get(self.live_server_url)

        time.sleep(3)  # Makes the user actually see the page before proceeding

        self.assertIn(
            'Good Morning!',
            self.selenium.find_elements_by_tag_name('h4')[0].text
        )

    def test_tweetbox_is_exist(self):
        self.selenium.get(self.live_server_url)
        tweet_box = self.selenium.find_element_by_id('id_tweet_text')

        self.assertEqual(tweet_box.get_attribute('class'), "form-control")

    def test_tweet_submit_button_is_exist(self):
        self.selenium.get(self.live_server_url)
        tweet_button = self.selenium.find_element_by_tag_name('button')

        self.assertEqual(tweet_button.text, 'Tweet!')    

    def test_user_is_twitting(self):
        self.selenium.get(self.live_server_url)

        tweet_box = self.selenium.find_element_by_id('id_tweet_text')
        tweet_box.send_keys('Great Day')

        self.selenium.find_element_by_class_name('twittum-btn').click()

        time.sleep(3)

        self.assertEqual(
            self.selenium.find_elements_by_id('myTweet')[0].text,
            'Great Day'
        )

    def test_copyright_is_exist(self):
        self.selenium.get(self.live_server_url)

        cr = self.selenium.find_element_by_class_name('cr')

        self.assertIn('Iqrar Agalosi Nureyza © 2019', cr.text)

