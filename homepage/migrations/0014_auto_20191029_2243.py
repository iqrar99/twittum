# Generated by Django 2.2.6 on 2019-10-29 15:43

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0013_auto_20191029_2242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='tweet_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 10, 29, 22, 43, 55, 173043)),
        ),
    ]
