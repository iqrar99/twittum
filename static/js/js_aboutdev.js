$(document).ready(function () {
    $("#btn1").click(function () {
        $("#box1").slideToggle("slow");
    });

    $("#btn2").click(function () {
        $("#box2").slideToggle("slow");
    });

    $("#btn3").click(function () {
        $("#box3").slideToggle("slow");
    });

    // change the website theme
    $(".btn-colormode").click(function () {
        $(".cover-bg").toggleClass("cover-bg2");
        $('#colormode1').toggle();
        $('#colormode2').toggle();

        $('#text1').toggle();
        $('#text1-2').toggle();

        $('#text2').toggle();
        $('#text2-2').toggle();

        $('#text3').toggle();
        $('#text3-2').toggle();
    })
});